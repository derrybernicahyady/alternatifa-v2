<script type="module">
// Import the functions you need from the SDKs you need
import { initializeApp } from "https://www.gstatic.com/firebasejs/10.6.0/firebase-app.js";
import { getMessaging, getToken } from "https://www.gstatic.com/firebasejs/10.6.0/firebase-messaging.js"
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
    apiKey: "AIzaSyD8vWTeRII2KW5S6Jb_3Kdtcq-xd9IabAo",
    authDomain: "alternatifa-ef319.firebaseapp.com",
    projectId: "alternatifa-ef319",
    storageBucket: "alternatifa-ef319.appspot.com",
    messagingSenderId: "78042738337",
    appId: "1:78042738337:web:b1ebb5f1a87adbd213cae0",
    measurementId: "G-Z0TVHP125H"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);

// subsequent calls to getToken will return from cache.
const messaging = getMessaging();
getToken(messaging, { vapidKey: 'BEpPRtndyKpM2VOfNtW-x9siiFZl2eYisyB0EN0CoOH7XB0yaNRV3_0TxT-V2JMkbe_q0Op9C7tudnG_4dsTLH4' }).then((currentToken) => {
if (currentToken) {
    // Send the token to your server and update the UI if necessary
    // ...
} else {
    // Show permission request UI
    console.log('No registration token available. Request permission to generate one.');
    // ...
}
}).catch((err) => {
    console.log('An error occurred while retrieving token. ', err);
// ...
});


</script>