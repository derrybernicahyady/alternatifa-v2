importScripts('https://www.gstatic.com/firebasejs/10.6.0/firebase-app-compat.js');
importScripts('https://www.gstatic.com/firebasejs/10.6.0/firebase-messaging-compat.js');

var config = {
    apiKey: "AIzaSyD8vWTeRII2KW5S6Jb_3Kdtcq-xd9IabAo",
    authDomain: "alternatifa-ef319.firebaseapp.com",
    projectId: "alternatifa-ef319",
    storageBucket: "alternatifa-ef319.appspot.com",
    messagingSenderId: "78042738337",
    appId: "1:78042738337:web:b1ebb5f1a87adbd213cae0",
    measurementId: "G-Z0TVHP125H"
};

firebase.initializeApp(config);

const messaging = firebase.messaging();

// https://firebase.google.com/docs/cloud-messaging/concept-options
messaging.onBackgroundMessage(function(payload) {
  console.log('[firebase-messaging-sw.js] Received background message ', payload);
  // Customize notification here
  const notificationTitle = 'Background Message Title';
  const notificationOptions = {
    body: 'Background Message body.',
    icon: '/firebase-logo.png'
  };

  self.registration.showNotification(notificationTitle, notificationOptions);
});